package musicserver.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.util.Calendar;

/**
 * Created by caligone on 10/10/14.
 */
public class MusicService implements IMusicService {

    public byte[] getMidiOfTheDay() throws RemoteException {
        System.out.println("getMidiOfTheDay called");
        File repertoire = new File("midi");
        File[] files = repertoire.listFiles();
        File midiFile = files[Calendar.DAY_OF_MONTH%files.length];
        try
        {
            byte buffer[] = new byte[(int)midiFile.length()];
            BufferedInputStream input = new BufferedInputStream(new FileInputStream(midiFile));
            input.read(buffer,0,buffer.length);
            input.close();
            return buffer;
        }
        catch(Exception e)
        {
            System.out.println("FileImpl : "+e.getMessage());
            e.printStackTrace();
            return null;
        }

    }
}
