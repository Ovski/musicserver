package musicserver;

import musicserver.service.IMusicService;
import musicserver.service.MusicService;

import java.io.File;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;

/**
 * Created by caligone on 06/10/14.
 */
public class MusicServer {

    private static int PORT = 25565;

    public MusicServer() throws RemoteException {
        // Create the bind object
        IMusicService service = (IMusicService) UnicastRemoteObject.exportObject(new MusicService(), MusicServer.PORT);

        // Bind the port
        Registry registry = LocateRegistry.createRegistry(MusicServer.PORT);

        // Bind the service
        registry.rebind("MusicService", service);

        System.out.println("MusicServer listening on port " + PORT + "...");
    }
}
