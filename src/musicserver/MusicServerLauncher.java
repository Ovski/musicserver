package musicserver;

import java.rmi.RemoteException;

/**
 * Created by caligone on 06/10/14.
 */
public class MusicServerLauncher {
    private static MusicServer musicServer;

    public static void main(String[] args) {
        try {
            MusicServerLauncher.musicServer = new MusicServer();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
